﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Blog.Models;
using System.IO;
using Newtonsoft.Json;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            string folderpath = "/img/home-banners/";
            ViewData["banners"] = new DirectoryInfo($"./wwwroot/{folderpath}").GetFiles().Select(c => String.Concat(folderpath, "/", c.Name)).ToList();
            return View();
        }


        [Route("graphic")]
        public IActionResult graphic()
        {

            ViewData["images"] = new DirectoryInfo("./wwwroot/img/graphic").GetFiles().Where(c => !c.Name.Contains(".json")).Select(c => String.Concat("/img/graphic/", c.Name)).ToList();

            return View();
        }

        [Route("lettering/{imageUrl}")]
        [Route("photography/{imageUrl}")]
        [Route("graphic/{imageUrl}")]
        [Route("mural/{imageUrl}")]
        public IActionResult imageDetail(string imageurl)
        {
            var foldername = Request.Path.Value.Split('/')[1];

            var filename = new DirectoryInfo("./wwwroot/img/" + foldername).GetFiles().Where(c => c.Name.Contains(imageurl + ".json")).Select(c => String.Concat(c.FullName)).FirstOrDefault();
            imagedetail model = new imagedetail();

            if (System.IO.File.Exists(filename))
            {
                model = JsonConvert.DeserializeObject<imagedetail>(System.IO.File.ReadAllText(filename));
                model.img = String.Concat("/img/", foldername, "/", Path.GetFileName(filename)).Replace(".json", ".jpg");

            }
            else
            {
                model.img = String.Concat("/img/", foldername, "/", imageurl,".jpg");
            }

            return View(model);
        }

        [Route("about")]
        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
