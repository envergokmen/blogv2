﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


$(document).ready(function () {

    $activeMenu = $("a[href*='" + location.pathname.split('/')[1] + "']");
    $activeMenu.closest("li").addClass("active");


    $('.grid').imagesLoaded(function () {
        console.log("images are loaded");
        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: 270,
            gutter: 10,
            isAnimated: true
        });

    });

    $('.grid img1').click(function () {

        $('#modal-img').attr('src', $(this).attr('src'));
        $('#gallerymodal').modal('show');

    });



});
