﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blog.Models
{
    public class imagedetail
    {
        public string title { get; set; }
        public string client { get; set; }
        public string summary { get; set; }
        public string link { get; set; }
        public string date { get; set; }
        public string img { get; set; }

    }
}
